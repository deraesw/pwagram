importScripts("https://storage.googleapis.com/workbox-cdn/releases/4.1.0/workbox-sw.js");
importScripts('/src/js/idb.js');
importScripts('/src/js/indexedDB.js');
importScripts('/src/js/constant.js');

workbox.routing.registerRoute(
    /^https:\/\/fonts\.(?:googleapis|gstatic)\.com/,
    new workbox.strategies.StaleWhileRevalidate({
        cacheName: 'google-fonts',
        plugins: [
            new workbox.expiration.Plugin({
                maxAgeSeconds: 60 * 60 * 24 * 30,//One month.
                maxEntries: 10,
            }),
        ]
    }),
);

workbox.routing.registerRoute(
    'https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.3.0/material.indigo-pink.min.css',
    new workbox.strategies.StaleWhileRevalidate({
        cacheName: 'material-css',
    }),
);

workbox.routing.registerRoute(
    /.*(?:firebasestorage\.googleapis)\.com/,
    new workbox.strategies.StaleWhileRevalidate({
        cacheName: 'post-images',
    }),
);

const handler = async ({url, event}) => {
    let response = await fetch(event.request);
    try {
        let cloneResponse = response.clone();
        await clearAllData('posts');
        let data = await cloneResponse.json();
        for (let key in data) {
            await writeData('posts', key, data[key]);
        }
    } catch (error) {
        console.log(error);
    }
    return response;
};

workbox.routing.registerRoute(firebasePostsUrl, handler);

workbox.routing.registerRoute((postData) => {
    return (postData.event.request.headers.get('accept').includes('text/html'));
}, async ({url, event}) =>{
    let response = await caches.match(event.request);
    if(response) {
        return response;
    } else {
        try {
            let responseFetch = await fetch(event.request);
            let cache = await caches.open('dynamic');
            cache.put(event.request.url, responseFetch.clone());
            return responseFetch;
        } catch (error) {
            return await caches.match(workbox.precaching.getCacheKeyForURL('/offline.html'));
        }
    }
});

workbox.precaching.precacheAndRoute([
  {
    "url": "404.html",
    "revision": "0a27a4163254fc8fce870c8cc3a3f94f"
  },
  {
    "url": "favicon.ico",
    "revision": "2cab47d9e04d664d93c8d91aec59e812"
  },
  {
    "url": "index.html",
    "revision": "099f2c8bd1cf144138a9a9aea533363b"
  },
  {
    "url": "manifest.json",
    "revision": "538fdcc3f750052da317f989793529cb"
  },
  {
    "url": "offline.html",
    "revision": "017b5ea6249805c82a7d293847a087c6"
  },
  {
    "url": "src/css/app.css",
    "revision": "1d4c79a1d1224fcdd63343290d6866bf"
  },
  {
    "url": "src/css/feed.css",
    "revision": "2797bf0559793ac4193544e9a9107e70"
  },
  {
    "url": "src/css/help.css",
    "revision": "1c6d81b27c9d423bece9869b07a7bd73"
  },
  {
    "url": "src/js/app.js",
    "revision": "707d16e1c6b98096809a4131fe536026"
  },
  {
    "url": "src/js/constant.js",
    "revision": "fd07de4e4d3656f2cfc500a3692276b3"
  },
  {
    "url": "src/js/feed.js",
    "revision": "287c1596e90274d977843615f9629ab5"
  },
  {
    "url": "src/js/fetch.js",
    "revision": "6b82fbb55ae19be4935964ae8c338e92"
  },
  {
    "url": "src/js/idb.js",
    "revision": "017ced36d82bea1e08b08393361e354d"
  },
  {
    "url": "src/js/indexedDB.js",
    "revision": "1e6a4a8e4cad0ca15adb1cede5d9fea9"
  },
  {
    "url": "src/js/material.min.js",
    "revision": "713af0c6ce93dbbce2f00bf0a98d0541"
  },
  {
    "url": "src/js/promise.js",
    "revision": "10c2238dcd105eb23f703ee53067417f"
  },
  {
    "url": "src/js/utils.js",
    "revision": "d4bd15f4291da64a6eed422f4cf5d49e"
  },
  {
    "url": "sw-base.js",
    "revision": "08dd139308a17dcb004a795988994910"
  },
  {
    "url": "sw.js",
    "revision": "d8435cfc3e9f0cffdc08f837f5fb9f79"
  },
  {
    "url": "src/images/main-image-lg.jpg",
    "revision": "31b19bffae4ea13ca0f2178ddb639403"
  },
  {
    "url": "src/images/main-image-sm.jpg",
    "revision": "c6bb733c2f39c60e3c139f814d2d14bb"
  },
  {
    "url": "src/images/main-image.jpg",
    "revision": "5c66d091b0dc200e8e89e56c589821fb"
  },
  {
    "url": "src/images/sf-boat.jpg",
    "revision": "0f282d64b0fb306daf12050e812d6a19"
  }
]);

self.addEventListener('sync', (event) => {
    console.log('[service worker] Background syncing', event);
    if (event.tag === 'sync-new-post') {
        event.waitUntil(
            readAllData('sync-posts')
                .then(data => {
                    for (let dt of data) {
                        let postData = new FormData();
                        postData.append('id', dt.id);
                        postData.append('title', dt.title);
                        postData.append('location', dt.location);
                        postData.append('file', dt.picture, dt.id + '.png');
                        postData.append('rawLocationLat', dt.rawLocation.lat);
                        postData.append('rawLocationLng', dt.rawLocation.lng);

                        fetch(firebaseFunctionPostUrl, {
                            method: 'POST',
                            body: postData
                        }).then(response => {
                            if (response.ok) {
                                response.json()
                                    .then((responseData) => {
                                        deleteItemFromData('sync-posts', responseData.id)
                                    });
                            }
                        })
                    }
                })
                .catch(error => {
                    console.log("Error", error);
                })
        );
    }
});

self.addEventListener('notificationClick', (event) => {
    let notification = event.notification;
    let action = event.action;

    if (action === 'confirm') {
        notification.close();
    } else {
        event.waitUntil(
            clients.matchAll()
                .then((cls) => {
                    let client = cls.find((c) => {
                        return c.visibilityState === 'visible';
                    });

                    if (client !== undefined) {
                        client.navigate(notification.data.url)
                            .catch(error => {
                                console.log(error);
                            });
                        client.focus();
                    } else {
                        clients.openWindow(notification.data.url);
                    }
                })
        );
        notification.close();
    }
});

self.addEventListener('notificationClose', (event) => {
    // let notification = event.notification;
});

self.addEventListener('push', (event) => {
    let data = {
        title: 'New!',
        content: ' Something new happened!',
        openUrl: '/'
    };

    if (event.data) {
        data = JSON.parse(event.data.text());
    }

    let options = {
        body: data.content,
        icon: '/src/images/icons/app-icon-96x96.png',
        data: {
            url: data.openUrl
        },
        badge: '/src/images/icons/app-icon-96x96.png',
    };
    event.waitUntil(
        self.registration.showNotification(data.title, options)
    );
});