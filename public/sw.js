importScripts('/src/js/idb.js');
importScripts('/src/js/indexedDB.js');
importScripts('/src/js/constant.js');

const CACHE_SHELL_NAME = 'app_shell_v45';
const CACHE_DYNAMIC_NAME = 'dynamic_v5';
const STATIC_FILES = [
    '/',
    '/index.html',
    '/offline.html',
    '/src/js/app.js',
    '/src/js/idb.js',
    '/src/js/utils.js',
    '/src/js/feed.js',
    '/src/js/promise.js',
    '/src/js/fetch.js',
    '/src/js/material.min.js',
    '/src/css/app.css',
    '/src/css/feed.css',
    '/src/images/main-image.jpg',
    'https://fonts.googleapis.com/css?family=Roboto:400,700',
    'https://fonts.googleapis.com/icon?family=Material+Icons',
    'https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.3.0/material.indigo-pink.min.css'
];

function trimCache(cacheName, maxItems) {
    caches.open(cacheName)
        .then(cache => {
            return cache.keys()
                .then(keys => {
                    if (keys.length >= maxItems) {
                        cache.delete(keys[0])
                            .then(
                                trimCache(cacheName, maxItems)
                            )
                    }
                });
        })

}

/**
 * Using async and await instead of promises
 * Still to test
 * @param cacheName
 * @param maxItems
 * @returns {Promise<void>}
 */
// async function trimCache(cacheName, maxItems) {
//     let cache = await caches.open(cacheName);
//     let keys = cache.keys;
//     if(keys >= maxItems) {
//         await caches.delete(keys[0]);
//         trimCache(cacheName, maxItems);
//     }
// }

self.addEventListener('install', (event) => {
    event.waitUntil(
        caches.open(CACHE_SHELL_NAME)
            .then((cache) => {
                cache.addAll(STATIC_FILES)
            })
    );
});

self.addEventListener('activate', (event) => {
    event.waitUntil(
        caches.keys()
            .then((keyList) => {
                return Promise.all(keyList.map(key => {
                    if (key !== CACHE_SHELL_NAME && key !== CACHE_DYNAMIC_NAME) {
                        return caches.delete(key);
                    }
                }));
            })
    );
    return self.clients.claim();
});

function isInArray(string, array) {
    for (let i = 0; i < array.length; i++) {
        if (array[i] === string) {
            return true;
        }
    }
    return false;
}

self.addEventListener('sync', (event) => {
    console.log('[service worker] Background syncing', event);
    if (event.tag === 'sync-new-post') {
        event.waitUntil(
            readAllData('sync-posts')
                .then(data => {
                    for (let dt of data) {
                        let postData = new FormData();
                        postData.append('id', dt.id);
                        postData.append('title', dt.title);
                        postData.append('location', dt.location);
                        postData.append('file', dt.picture, dt.id + '.png');
                        postData.append('rawLocationLat', dt.rawLocation.lat);
                        postData.append('rawLocationLng', dt.rawLocation.lng);

                        fetch(firebaseFunctionPostUrl, {
                            method: 'POST',
                            body: postData
                        }).then(response => {
                            if (response.ok) {
                                response.json()
                                    .then((responseData) => {
                                        deleteItemFromData('sync-posts', responseData.id)
                                    });
                            }
                        })
                    }
                })
                .catch(error => {
                    console.log("Error", error);
                })
        );
    }
});

//MIXED STRATEGY
//Handle offline mode as well
//------------------------------
self.addEventListener('fetch', (event) => {
    if (event.request.url.indexOf(firebasePostsMainUrl) > -1) {
        //CACHE THEN NETWORK STRATEGY
        event.respondWith(
            fetch(event.request)
                .then(response => {
                    let cloneResponse = response.clone();
                    clearAllData('posts')
                        .then(() => {
                            return cloneResponse.json();
                        })
                        .then(data => {
                            for (let key in data) {
                                writeData('posts', key, data[key]);
                            }
                        });
                    return response;
                })
        )
    } else if (isInArray(event.request.url, STATIC_FILES)) {
        //CACHE ONLY STRATEGY
        event.respondWith(
            caches.match(event.request)
        );
    } else {
        //CACHE THEN NETWORK STRATEGY
        event.respondWith(
            caches.match(event.request)
                .then((response) => {
                    if (response) {
                        return response;
                    } else {
                        return fetch(event.request)
                            .then((responseFetch) => {
                                return caches.open(CACHE_DYNAMIC_NAME)
                                    .then((cache) => {
                                        trimCache(CACHE_DYNAMIC_NAME, 30);
                                        cache.put(event.request.url, responseFetch.clone());
                                        return responseFetch;
                                    });
                            }).catch((error) => {
                                return caches.open(CACHE_SHELL_NAME)
                                    .then(cache => {
                                        if (event.request.headers.get('accept').includes('text/html')) {
                                            return cache.match('/offline.html');
                                        }
                                    });
                            });
                    }
                })
        )
    }
});


//CACHE THEN NETWORK STRATEGY
//------------------------------
// self.addEventListener('fetch', (event) => {
//     event.respondWith(
//         caches.open(CACHE_DYNAMIC_NAME)
//             .then(cache => {
//                 return fetch(event.request)
//                     .then(response => {
//                         cache.put(event.request, response.clone());
//                         return response;
//                     })
//             })
//     );
// });

//CACHE WITH NETWORK FALLBACK STRATEGY
//------------------------------
// self.addEventListener('fetch', (event) => {
//     event.respondWith(
//         caches.match(event.request)
//             .then((response) => {
//                 if (response) {
//                     return response;
//                 } else {
//                     return fetch(event.request)
//                         .then((responseFetch) => {
//                             return caches.open(CACHE_DYNAMIC_NAME)
//                                 .then((cache) => {
//                                     cache.put(event.request.url, responseFetch.clone());
//                                     return responseFetch;
//                                 });
//                         }).catch((error) => {
//                             return caches.open(CACHE_SHELL_NAME)
//                                 .then(cache => {
//                                    return cache.match('/offline.html');
//                                 });
//                         });
//                 }
//             })
//     );
// });

//NETWORK WITH CACHE FALLBACK STRATEGY
//-------------------------------
// self.addEventListener('fetch', (event) => {
//     event.respondWith(
//         fetch(event.request)
//             .then(response => {
//                 return caches.open(CACHE_DYNAMIC_NAME)
//                     .then((cache) => {
//                         cache.put(event.request.url, response.clone());
//                         return response;
//                     });
//             })
//             .catch(error => {
//                 return caches.match(event.request)
//             })
//     );
// });

//CACHE ONLY STRATEGY
//Try to handle everything from cache, don't call the network
//--------------------------
// self.addEventListener('fetch', (event) => {
//     event.respondWith(
//         caches.match(event.request)
//     );
// });

// NETWORK ONLY STRATEGY
// Normally don't use inside service worker
//---------------------------
// self.addEventListener('fetch', (event) => {
//     event.respondWith(
//         fetch(event.request)
//     );
// });


self.addEventListener('notificationClick', (event) => {
    let notification = event.notification;
    let action = event.action;

    if (action === 'confirm') {
        notification.close();
    } else {
        event.waitUntil(
            clients.matchAll()
                .then((cls) => {
                    let client = cls.find((c) => {
                        return c.visibilityState === 'visible';
                    });

                    if (client !== undefined) {
                        client.navigate(notification.data.url)
                            .catch(error => {
                                console.log(error);
                            });
                        client.focus();
                    } else {
                        clients.openWindow(notification.data.url);
                    }
                })
        );
        notification.close();
    }
});

self.addEventListener('notificationClose', (event) => {
    // let notification = event.notification;
});

self.addEventListener('push', (event) => {
    console.log('Push notification received');

    let data = {
        title: 'New!',
        content: ' Something new happened!',
        openUrl: '/'
    };
    if (event.data) {
        data = JSON.parse(event.data.text());
    }

    let options = {
        body: data.content,
        icon: '/src/images/icons/app-icon-96x96.png',
        data: {
            url: data.openUrl
        },
        // image: '/src/images/sf-boat.jpg',
        // dir: 'ltr',
        // lang: 'en-US',//BCP 47
        // vibrate: [
        //     100, //vibration
        //     50, //pause
        //     200 //vibration
        // ],
        badge: '/src/images/icons/app-icon-96x96.png',
        // tag: 'confirm-notification',
        // renotify: true,
        // actions: [
        //     {action: 'confirm', title: 'Okey', icon: '/src/images/icons/app-icon-96x96.png'},
        //     {action: 'cancel', title: 'Cancel', icon: '/src/images/icons/app-icon-96x96.png'}
        // ]
    };
    event.waitUntil(
        self.registration.showNotification(data.title, options)
    );
});