importScripts("https://storage.googleapis.com/workbox-cdn/releases/4.1.0/workbox-sw.js");
importScripts('/src/js/idb.js');
importScripts('/src/js/indexedDB.js');
importScripts('/src/js/constant.js');

workbox.routing.registerRoute(
    /^https:\/\/fonts\.(?:googleapis|gstatic)\.com/,
    new workbox.strategies.StaleWhileRevalidate({
        cacheName: 'google-fonts',
        plugins: [
            new workbox.expiration.Plugin({
                maxAgeSeconds: 60 * 60 * 24 * 30,//One month.
                maxEntries: 10,
            }),
        ]
    }),
);

workbox.routing.registerRoute(
    'https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.3.0/material.indigo-pink.min.css',
    new workbox.strategies.StaleWhileRevalidate({
        cacheName: 'material-css',
    }),
);

workbox.routing.registerRoute(
    /.*(?:firebasestorage\.googleapis)\.com/,
    new workbox.strategies.StaleWhileRevalidate({
        cacheName: 'post-images',
    }),
);

const handler = async ({url, event}) => {
    let response = await fetch(event.request);
    try {
        let cloneResponse = response.clone();
        await clearAllData('posts');
        let data = await cloneResponse.json();
        for (let key in data) {
            await writeData('posts', key, data[key]);
        }
    } catch (error) {
        console.log(error);
    }
    return response;
};

workbox.routing.registerRoute(firebasePostsUrl, handler);

workbox.routing.registerRoute((postData) => {
    return (postData.event.request.headers.get('accept').includes('text/html'));
}, async ({url, event}) =>{
    let response = await caches.match(event.request);
    if(response) {
        return response;
    } else {
        try {
            let responseFetch = await fetch(event.request);
            let cache = await caches.open('dynamic');
            cache.put(event.request.url, responseFetch.clone());
            return responseFetch;
        } catch (error) {
            return await caches.match(workbox.precaching.getCacheKeyForURL('/offline.html'));
        }
    }
});

workbox.precaching.precacheAndRoute([]);

self.addEventListener('sync', (event) => {
    console.log('[service worker] Background syncing', event);
    if (event.tag === 'sync-new-post') {
        event.waitUntil(
            readAllData('sync-posts')
                .then(data => {
                    for (let dt of data) {
                        let postData = new FormData();
                        postData.append('id', dt.id);
                        postData.append('title', dt.title);
                        postData.append('location', dt.location);
                        postData.append('file', dt.picture, dt.id + '.png');
                        postData.append('rawLocationLat', dt.rawLocation.lat);
                        postData.append('rawLocationLng', dt.rawLocation.lng);

                        fetch(firebaseFunctionPostUrl, {
                            method: 'POST',
                            body: postData
                        }).then(response => {
                            if (response.ok) {
                                response.json()
                                    .then((responseData) => {
                                        deleteItemFromData('sync-posts', responseData.id)
                                    });
                            }
                        })
                    }
                })
                .catch(error => {
                    console.log("Error", error);
                })
        );
    }
});

self.addEventListener('notificationClick', (event) => {
    let notification = event.notification;
    let action = event.action;

    if (action === 'confirm') {
        notification.close();
    } else {
        event.waitUntil(
            clients.matchAll()
                .then((cls) => {
                    let client = cls.find((c) => {
                        return c.visibilityState === 'visible';
                    });

                    if (client !== undefined) {
                        client.navigate(notification.data.url)
                            .catch(error => {
                                console.log(error);
                            });
                        client.focus();
                    } else {
                        clients.openWindow(notification.data.url);
                    }
                })
        );
        notification.close();
    }
});

self.addEventListener('notificationClose', (event) => {
    // let notification = event.notification;
});

self.addEventListener('push', (event) => {
    let data = {
        title: 'New!',
        content: ' Something new happened!',
        openUrl: '/'
    };

    if (event.data) {
        data = JSON.parse(event.data.text());
    }

    let options = {
        body: data.content,
        icon: '/src/images/icons/app-icon-96x96.png',
        data: {
            url: data.openUrl
        },
        badge: '/src/images/icons/app-icon-96x96.png',
    };
    event.waitUntil(
        self.registration.showNotification(data.title, options)
    );
});