let shareImageButton = document.querySelector('#share-image-button');
let createPostArea = document.querySelector('#create-post');
let closeCreatePostModalButton = document.querySelector('#close-create-post-modal-btn');
let sharedMomentsArea = document.querySelector('#shared-moments');
let form = document.querySelector('form');
let inputTitle = document.querySelector('#title');
let inputLocation = document.querySelector('#location');

let videoPlayer = document.querySelector('#player');
let canvasElement = document.querySelector('#canvas');
let captureButton = document.querySelector('#capture-btn');
let imagePicker = document.querySelector('#image-picker');
let imagePickerArea = document.querySelector('#pick-image');

let locationButton = document.querySelector('#location-btn');
let locationLoader = document.querySelector('#location-loader');

let picture;
let fetchLocation = {lat: 0, lng: 0};

let networkDataReceived = false;

function openCreatePostModal() {
    createPostArea.style.display = 'block';
    setTimeout(() => {
        createPostArea.style.transform = 'translateY(0)';
    }, 1);
    initializeMedia();
    initializeLocation();
}

function initializeMedia() {
    if (!('mediaDevices' in navigator)) {
        navigator.mediaDevices = {};
    }

    if (!('getUserMedia' in navigator.mediaDevices)) {
        navigator.mediaDevices.getUserMedia = function (constraints) {
            let getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
            if (!getUserMedia) {
                return Promise.reject(new Error('getUserMedia is not implemented!'));
            }

            return new Promise((resolve, reject) => {
                getUserMedia.call(navigator, constraints, resolve, reject);
            });
        };
    }
    navigator.mediaDevices.getUserMedia({video: true})
        .then(stream => {
            videoPlayer.srcObject = stream;
            videoPlayer.style.display = 'block';
        })
        .catch(error => {
            videoPlayer.style.display = 'none';
            captureButton.style.display = 'none';
            imagePickerArea.style.display = 'block';
        });
}

function initializeLocation() {
    if (!('geolocation' in navigator)) {
        locationButton.style.display = 'none';
    }
}

captureButton.addEventListener('click', event => {
    canvasElement.style.display = 'block';
    videoPlayer.style.display = 'none';
    captureButton.style.display = 'none';

    let context = canvasElement.getContext('2d');
    context.drawImage(videoPlayer, 0, 0, canvas.width, videoPlayer.videoHeight / (videoPlayer.videoWidth / canvas.width));
    stopVideo();
    picture = dataURItoBlob(canvasElement.toDataURL());
});

imagePicker.addEventListener('change', event => {
    picture = event.target.files[0];
});

locationButton.addEventListener('click', event => {

    displayLocationLoader(true);

    navigator.geolocation.getCurrentPosition(
        position => {
            displayLocationLoader(false);
            console.log(position);
            fetchLocation = {lat: position.coords.latitude, lng: position.coords.longitude};
            inputLocation.value = 'lat' + fetchLocation.lat;
            document.querySelector('#manual-location').classList.add('is-focused');
        },
        error => {
            console.log(error);
            displayLocationLoader(false);
            alert('Could not fetch location');
            fetchLocation = {lat: 0, lng: 0};
        },
        {timeout: 5000}
    );
});

function closeCreatePostModal() {
    imagePickerArea.style.display = 'none';
    videoPlayer.style.display = 'none';
    canvasElement.style.display = 'none';
    captureButton.style.display = 'inline';

    displayLocationLoader(true);
    stopVideo();

    setTimeout(() => {
        createPostArea.style.transform = 'translateY(100vh)';
    }, 1);
}

function displayLocationLoader(showing) {
    if (showing) {
        locationButton.style.display = 'none';
        locationLoader.style.display = 'block';
    } else {
        locationButton.style.display = 'inline';
        locationLoader.style.display = 'none';
    }
}

function stopVideo() {
    if (videoPlayer.srcObject) {
        videoPlayer.srcObject
            .getVideoTracks()
            .forEach(track => {
                track.stop()
            });
    }
}

// Keep for references - Caching on demand
// function onSaveButtonClick(event) {
//     console.log('[FEED]click');
//     if('caches' in window) {
//         caches.open('user-requested')
//             .then((cache) => {
//                 cache.addAll([
//                     'https://httpbin.org/get',
//                     '/src/images/sf-boat.jpg'
//                 ])
//             });
//     }
// }

function clearCards() {
    while (sharedMomentsArea.hasChildNodes()) {
        sharedMomentsArea.removeChild(sharedMomentsArea.lastChild);
    }
}

function createCard(data) {
    let cardWrapper = document.createElement('div');
    cardWrapper.className = 'shared-moment-card mdl-card mdl-shadow--2dp';

    let cardTitle = document.createElement('div');
    cardTitle.className = 'mdl-card__title';
    cardTitle.style.backgroundImage = 'url(' + data.image + ')';
    cardTitle.style.backgroundSize = 'cover';
    cardWrapper.appendChild(cardTitle);

    let cardTitleTextElement = document.createElement('h2');
    cardTitleTextElement.className = 'mdl-card__title-text';
    cardTitleTextElement.textContent = data.title;
    cardTitle.appendChild(cardTitleTextElement);

    let cardSupportingText = document.createElement('div');
    cardSupportingText.className = 'mdl-card__supporting-text';
    cardSupportingText.textContent = data.location;
    cardSupportingText.style.textAlign = 'center';
    cardWrapper.appendChild(cardSupportingText);

    // Keep for references - Button to cache on demand
    // let cardSaveButton = document.createElement('button');
    // cardSaveButton.textContent = 'save';
    // cardSaveButton.addEventListener('click', onSaveButtonClick);
    // cardSupportingText.append(cardSaveButton);

    componentHandler.upgradeElement(cardWrapper);
    sharedMomentsArea.appendChild(cardWrapper);
}

function updateUi(data) {
    for (const post of data) {
        createCard(post);
    }
}

function parseResultDataToArray(data) {
    let dataArray = [];
    for (let key in data) {
        dataArray.push(data[key]);
    }
    return dataArray;
}

async function fetchData() {
    try {
        let response = await fetch(firebasePostsUrl);
        let data = await response.json();
        if (data) {
            networkDataReceived = true;
            clearCards();
            updateUi(parseResultDataToArray(data));
        }
    } catch (error) {
        console.log('[FEED] fetchData', error);
    }

}

fetchData().catch(error => {
    console.log('Errors', error);
});

// fetch(url)
//     .then(function (response) {
//         return response.json();
//     })
//     .then(function (data) {
//         networkDataReceived = true;
//         clearCards();
//         updateUi(parseResultDataToArray(data));
//     });


if ('indexedDB' in window) {
    readAllData('posts').then(data => {
        if (!networkDataReceived) {
            updateUi(data);
        }
    });
}

function sendData() {
    let id = new Date().toISOString();
    let postData = new FormData();
    postData.append('id', id);
    postData.append('title', inputTitle.value);
    postData.append('location', inputLocation.value);
    postData.append('file', picture, id + '.png');
    postData.append('rawLocationLat', fetchLocation.lat);
    postData.append('rawLocationLng', fetchLocation.lng);

    fetch(firebasePostsUrl, {
        method: 'POST',
        body: postData
    }).then(response => {

    })
}

shareImageButton.addEventListener('click', openCreatePostModal);

closeCreatePostModalButton.addEventListener('click', closeCreatePostModal);

form.addEventListener('submit', (event) => {
    event.preventDefault();
    if (inputTitle.value.trim() === '' || inputLocation.value.trim() === '') {
        return;
    }
    closeCreatePostModal();

    if ('serviceWorker' in navigator && 'SyncManager' in window) {
        navigator.serviceWorker.ready
            .then(serviceWorker => {
                const post = {
                    id: new Date().toISOString(),
                    title: inputTitle.value,
                    location: inputLocation.value,
                    picture: picture,
                    rawLocation: fetchLocation
                };
                writeData('sync-posts', post.id, post)
                    .then(() => {
                        return serviceWorker.sync.register('sync-new-post');
                    })
                    .then(() => {
                        let snackbarContainer = document.querySelector('#confirmation-toast');
                        let data = {
                            message: 'Your post was saved for syncing!'
                        };
                        snackbarContainer.MaterialSnackbar.showSnackbar(data);
                    })
                    .catch(error => {
                        console.log(error);
                    })
            })
    } else {
        sendData();
    }
});