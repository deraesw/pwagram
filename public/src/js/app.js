//App Install Prompt
let deferredPrompt;
let enableNotificationsButtons = document.querySelectorAll('.enable-notifications');
let installPromptButtons = document.querySelectorAll('.install_prompt');

if (!window.Promise) {
    console.log('[APP] using browser that do not support promises');
    window.Promise = Promise;
} else {
    console.log('[APP] using browser that support promises');
}

if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
        navigator.serviceWorker
            .register('service-worker.js')
            .then(() => {
                console.log('[APP] service worker registered!');
            })
            .catch(err => {
                console.log('[APP] Error: ', err);
            });
    });
}

if ('Notification' in window && 'serviceWorker' in navigator) {
    for (let i = 0; i < enableNotificationsButtons.length; i++) {
        enableNotificationsButtons[i].style.display = 'inline-block';
        enableNotificationsButtons[i].addEventListener('click', askForNotificationPermission);
    }
}

function askForNotificationPermission() {
    Notification.requestPermission((result) => {
        if (result !== 'granted') {

        } else {
            //displayConfirmNotification();
            configurePushSub();
        }
    })
}

function displayConfirmNotification() {
    if ('serviceWorker' in navigator) {
        let options = {
            body: 'You successfully subscribed to our Notification service!',
            icon: '/src/images/icons/app-icon-96x96.png',
            image: '/src/images/sf-boat.jpg',
            dir: 'ltr',
            lang: 'en-US',//BCP 47
            vibrate: [
                100, //vibration
                50, //pause
                200 //vibration
            ],
            badge: '/src/images/icons/app-icon-96x96.png',
            tag: 'confirm-notification',
            renotify: true,
            actions: [
                {action: 'confirm', title: 'Okey', icon: '/src/images/icons/app-icon-96x96.png'},
                {action: 'cancel', title: 'Cancel', icon: '/src/images/icons/app-icon-96x96.png'}
            ]
        };

        navigator.serviceWorker.ready
            .then((swreg) => {
                swreg.showNotification('Successfully subscribed', options);
            });
    }
}

async function configurePushSub() {
    console.log('configurePushSub');
    if (!('serviceWorker' in navigator)) {
        return;
    }
    try {
        let swreg = await navigator.serviceWorker.ready;
        let sub = await swreg.pushManager.getSubscription();
        console.log('configurePushSub - part 1');
        if (!sub) {
            console.log('configurePushSub - part 2');
            let convertedVapidKey = urlBase64ToUint8Array(vapidPublicKey);
            let newSub = await swreg.pushManager.subscribe({
                userVisibleOnly: true,
                applicationServerKey: convertedVapidKey
            });
            console.log('configurePushSub - part 3');
            let response = await fetch(firebaseSubscriptionUrl, {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(newSub)
            });
            if (response.ok) {
                displayConfirmNotification();
            }
        } else {

        }
    } catch (e) {
        console.log('error', e);
    }

}

window.addEventListener('beforeinstallprompt', (event) => {
    console.log('[APP] before install prompt');
    // Prevent Chrome 67 and earlier from automatically showing the prompt
    event.preventDefault();
    // Stash the event so it can be triggered later.
    deferredPrompt = event;
});

window.addEventListener('appinstalled', (event) => {
    console.log('[APP] app installed');
});

for (let i = 0; i < installPromptButtons.length; i++) {
    installPromptButtons[i].addEventListener('click', (event) => {
        displayPromptHomeInstall(event)
            .catch(error => {
                console.log('[APP] Errors:', error);
            })
    });
}

async function displayPromptHomeInstall(event) {
    if (deferredPrompt) {
        deferredPrompt.prompt();
        let choiceResult = await deferredPrompt.userChoice;
        if (choiceResult.outcome === 'accepted') {
            console.log('[FEED] User accepted the A2HS prompt');
        } else {
            console.log('[FEED] User dismissed the A2HS prompt');
        }
        deferredPrompt = null;
    } else {
        console.log('[FEED] not deferred prompt')
    }
}