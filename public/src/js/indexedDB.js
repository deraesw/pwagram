
let dbPromise = idb.open('posts-store', 1, (db) => {
    if (!db.objectStoreNames.contains('posts')) {
        db.createObjectStore('posts', {KeyPath: 'id'});
    }

    if (!db.objectStoreNames.contains('sync-posts')) {
        db.createObjectStore('sync-posts', {KeyPath: 'id'});
    }
});

function writeData(storeName, keyIndex, data) {
    return dbPromise
        .then(db => {
            let tx = db.transaction(storeName, 'readwrite');
            let store = tx.objectStore(storeName);
            store.put(data, keyIndex);
            return tx.complete;
        });
}

function readAllData(storeName) {
    return dbPromise
        .then(db => {
            let tx = db.transaction(storeName, 'readonly');
            let store = tx.objectStore(storeName);
            return store.getAll();
        });
}

function clearAllData(storeName) {
    return dbPromise
        .then(db => {
            let tx = db.transaction(storeName, 'readwrite');
            let store = tx.objectStore(storeName);
            store.clear();
            return tx.complete;
        });
}

function deleteItemFromData(storeName, key) {
    return dbPromise
        .then(db => {
            let tx = db.transaction(storeName, 'readwrite');
            let store = tx.objectStore(storeName);
            store.delete(key);
            return tx.complete;
        });
}