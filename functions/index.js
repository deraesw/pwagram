const functions = require('firebase-functions');
const admin = require('firebase-admin');
const cors = require('cors')({origin: true});
const webPush = require('web-push');
const fs = require('fs');
const UUID = require('uuid-v4');
const os = require("os");
const Busboy = require("busboy");
const path = require('path');

const serviceAccount = require("./firebase_key.json");

const gcconfig = {
    projectId: functions.config().fb.project_id,
    keyFileName: 'firebase_key.json'
};

const gcs = require("@google-cloud/storage")(gcconfig);

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: functions.config().fb.database_url
});

exports.storePostData = functions.https.onRequest((request, response) => {
    cors(request, response, () => {
        let uuid = UUID();

        const busboy = new Busboy({headers: request.headers});
        let upload;
        const fields = {};

        busboy.on("file", (fieldname, file, filename, encoding, mimetype) => {
            const filepath = path.join(os.tmpdir(), filename);
            upload = { file: filepath, type: mimetype };
            file.pipe(fs.createWriteStream(filepath));
        });

        busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
            fields[fieldname] = val;
        });

        busboy.on("finish", () => {
            let bucket = gcs.bucket(functions.config().fb.bucket_name);
            let options = {
                uploadType: 'media',
                metadata: {
                    metadata: {
                        contentType: upload.type,
                        firebaseStorageDownloadTokens: uuid
                    }
                }
            };
            bucket.upload(upload.file, options, (error, file) => {
                if (!error) {
                    admin.database()
                        .ref('posts')
                        .push({
                            id: fields.id,
                            title: fields.title,
                            location: fields.location,
                            rawLocation: {
                                lat: fields.rawLocationLat,
                                lng: fields.rawLocationLng
                            },
                            image: 'https://firebasestorage.googleapis.com/v0/b/'
                                + bucket.name + '/o/'
                                + encodeURIComponent(file.name)
                                + '?alt=media&token=' + uuid
                        })
                        .then(() => {
                            webPush.setVapidDetails(
                                'mailto:test@test.com',
                                functions.config().vapid.public,
                                functions.config().vapid.private);

                            return admin
                                .database()
                                .ref('subscription')
                                .once('value');
                        })
                        .then(subscription => {
                            subscription.forEach((sub) => {
                                let pushConfig = {
                                    endpoint: sub.val().endpoint,
                                    keys: {
                                        auth: sub.val().keys.auth,
                                        p256dh: sub.val().keys.p256dh
                                    }
                                };

                                webPush.sendNotification(pushConfig, JSON.stringify({
                                    title: 'New post',
                                    content: 'New post added!',
                                    openUrl: '/help'
                                })).catch(error => {
                                    console.log(error);
                                });
                            });
                            response.status(201).json({
                                message: 'Data stored',
                                id: fields.id,
                                openUrl: '/help'
                            });
                        })
                        .catch(error => {
                            response.status(500).json({
                                error: error
                            });
                        });
                } else {
                    response.status(500).json({
                        error: error
                    });
                }
            });

        });

        busboy.end(request.rawBody);
    });
});

